import React from 'react';

const MappedData = ({currElem}) => {
    return (
        <div className="col-10 col-md-6 mt-5 ">
        <div className="card p-2">
          <div className="d-flex align-items-center">
              <strong className="platform">{currElem.platform} </strong>
            <div className="ml-3 w-100">
              <h4 className="mb-0 mt-0 textLeft">{currElem.title}</h4>
              {/* <span className="text-left">{type }</span> */}
              <div className="p-1 mt-5 bg-primary d-flex justify-content-between rounded text-white stats">
                <div className="d-flex flex-column">
                  <span className="articles">Score</span>{" "}
                  <span className="number1">{currElem.score}</span>{" "}
                </div>
                <div className="d-flex flex-column">
                  <span className="followers">Genre</span>{" "}
                  <span className="number2">{currElem.genre}</span>{" "}
                </div>
                <div className="d-flex flex-column">
                  <span className="rating">Editors</span>{" "}
                  <span className="number3">{currElem.editors_choice}</span>{" "}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
}

export default MappedData
