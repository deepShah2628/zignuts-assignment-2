import React,{useState} from "react";
import MappedData from "./MappedData";
import JSONDATA from "../JSON/JSONDATA.json";
import { Dropdown } from "semantic-ui-react";


const CardComponent = ({ gameData }) => {
  const [searchTerm, setSearchTerm] = useState("");
  const [listGames, setListGames] = useState(JSONDATA);
  const [games, setGames] = useState({listGames:[]})
  
 const onSortByPlatformAsc = () => {
    var sortByPlatform = listGames.sort((a, b) =>
      a.platform.localeCompare(b.platform)
    );
    setGames({ listGames: sortByPlatform });
  };

  const onSortByPlatformDesc = () => {
    var sortByPlatform = listGames.sort((a, b) =>
      a.platform.localeCompare(b.platform)
    );
    setGames({ listGames: sortByPlatform.reverse() });
  };
  
  return (
    <>
      <h2>List of Games</h2>
      <div className="Search">
        <input
          className="mx-5 col-10 Search"
          type="text"
          placeholder="Search..."
          align="center"
          onChange={event=>{setSearchTerm(event.target.value)}}
        />
      </div>
      <Dropdown className="sort-dropdown" item text="Sort By Platform">
                <Dropdown.Menu>
                  <Dropdown.Item 
                    className="asc-desc-sort"
                    icon="sort alphabet down"
                    text="Sort by asc"
                    onClick={onSortByPlatformAsc}
                  />
                  <Dropdown.Item
                   className="asc-desc-sort"
                    icon="sort alphabet up"
                    text="Sort by desc"
                    onClick={onSortByPlatformDesc}
                  />
                </Dropdown.Menu>
              </Dropdown>
      <div className="container-fluid mt-5">
        <div className="row text-center">
          {JSONDATA.filter((currElem)=>{
            if(searchTerm===""){
              return currElem;
            }else if(currElem.title.toLowerCase().includes(searchTerm.toLowerCase())){
              return currElem;
            }
          }).map((currElem , key) => {
            return <MappedData key={key} currElem={currElem} />;
          })}
        </div>
         
      </div>
    </>
  );
};

export default CardComponent;
