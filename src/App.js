import './App.css';
import React from 'react';
import SeachingComponent from './components/SearchingComponent';


function App() {
  return (
    <>
      <SeachingComponent />
    </>
  );
}

export default App;
